import express, { json } from "express";
import firstRouter from "./src/router/firstRouter.js";
import bikeRouter from "./src/router/bikeRouter.js";
import traineeRouter from "./src/router/traineeRouter.js";
import productRouter from "./src/router/productRouter.js";
import connectToMongoDb from "./src/connectToDb/connectToMongodb.js";
import userRouter from "./src/router/userRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import adminRouter from "./src/router/adminRouter.js";
import fileRouter from "./src/router/fileRouter.js";
import cors from "cors";
import { config } from "dotenv";
import collegeRouter from "./src/router/collegeRouter.js";
import webUserRouter from "./src/router/webUserRouter.js";
// making express application

config();

let expressApp = express();
expressApp.use(cors());
expressApp.use(json());

expressApp.use(express.static("./public"));
expressApp.use("/name", firstRouter);
expressApp.use("/bike", bikeRouter);
expressApp.use("/trainee", traineeRouter);
expressApp.use("/product", productRouter);
expressApp.use("/user", userRouter);
expressApp.use("/review", reviewRouter);
expressApp.use("/admin", adminRouter);
expressApp.use("/file", fileRouter);
expressApp.use("/college", collegeRouter);
expressApp.use("/webUser", webUserRouter);

connectToMongoDb();

//attach port
expressApp.listen(8000, () => {
  console.log("application is listening at port 8000");
});
