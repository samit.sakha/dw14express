import { Schema } from "mongoose";


/*  */

let productSchema = Schema({
    name:{
        type:String,
        lowercase:true,
        // uppercase:true,
        trim:true,
        required: true,
    },
    price:{
        type: Number,
        required: true,
    },
    quantity:{
        type: Number,
        required: true,
    },
    description:{
        type: String,
        required: false,
    },
    isAvailable:{
        type:Boolean,
        // default:true,
        required: true,
    }
})

export default productSchema