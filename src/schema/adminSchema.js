import { Schema } from "mongoose"



let adminSchema = Schema({
    name: {
          type: String,
          required: [true,"name field is required"],
          lowercase: true,
          trim: true,
          minlength:[3,"name must be at least 3 character long"],
          maxLength:[30,"name must be at most 30 character"],

        },

    password: {
          type: String,
          required: true,
          minlength:[8,"password must be between 8 to 20 character"],
          maxLength:[20,"password must be between 8 to 20 character"],

          validate:(value)=> {
            let hasNumber = /\d/.test(value)
            if (!hasNumber){
                let error = new Error ("must contain one number")
                throw error
            }
            let hasCapitalLetter= /[A-Z]/.test(value)
            if (!hasCapitalLetter){
                let error = new Error ("must contain one capital letter")
                throw error
            }
            let hasSmallLetter= /[a-z]/.test(value)
            if (!hasSmallLetter){
                let error = new Error ("must contain one small letter")
                throw error
            }
            let hasSymbol = /[^\w\s]/.test(value)
            if (!hasSymbol){
                let error = new Error ("must contain one symbol")
                throw error
            }
            let noNumOrSym = /^[a-zA-Z].*$/.test(value) 
            if (!noNumOrSym){
                let error = new Error ("must not start with number or symbol")
                throw error
            }
    

          }

        },

    phoneNumber: {
          type: Number,
          required: true,
          lowercase: true,
          trim: true,
                       
        validate:(value)=>{
            let strPhoneNumber = String(value)
            let strPhoneNumberLength = strPhoneNumber.length
            if (strPhoneNumberLength !==10){
                let error = new Error ("Phone number must be exact 10 digits")
                throw error
            }
        }
        },
    roll: {
          type: Number,
          required: true,
          trim: true,
          min:[20,"roll must be greater than 20"],
          max:[30,"roll must be less than 30"],
        },
    isMarried: {
          type: Boolean,
          required: true,
          trim: true,
        },
    spouseName: {
          type: String,
          required: true,
          trim: true,
        },
    email: {
          type: String,
          required: true,
          trim: true,
        },
    gender: {
          type: String,
          required: true,
          lowercase: true,
          trim: true,
          
          validate: (value)=> {
            if (value ==="female" || value==="male" || value==="other"){

            } else {
                let error = new Error("gender must be male, female or other")
                throw error
            }
          }



        },
    dob:{
          type:Date,
          required: true,
          trim: true,
        },
    location:{
        country:{
        type:String,
        },
    address:{
        type:String,
    }
    },
    favTeacher:[
        {
            type:String,
        }
    ],
    favSubject:[
        {
            bookName:{
                type: String,
            },
            bookAuthor:{
                type:String,
            }
        },
    ]
      
})

export default adminSchema