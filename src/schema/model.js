import { model } from "mongoose";

import userSchema from "./userSchema.js";
import productSchema from "./productSchema.js";
import reviewSchema from "./reviewSchema.js";
import adminSchema from "./adminSchema.js";
import collegeSchema from "./collegeSchema.js";
import webUserSchema from "./webUserSchema.js";

export let Products = model("Products", productSchema)
export let Users = model("Users", userSchema)
export let Reviews = model("Reviews", reviewSchema)
export let Admins = model("Admins", adminSchema)
export let Colleges = model("Colleges", collegeSchema)
export let WebUser = model("WebUser", webUserSchema)

// first letter capital