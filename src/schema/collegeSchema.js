import { Schema } from "mongoose";

/*  */

let collegeSchema = Schema({
  name: {
    type: String,
    lowercase: true,
    // uppercase:true,
    trim: true,
    required: true,
  },
  location: {
    type: String,
    lowercase: true,
    trim: true,
    required: true,
  },
  collegeImage: {
    type: String,
    required: false,
  },
});

export default collegeSchema;
