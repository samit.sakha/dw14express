import { Router } from "express";

let bikeRouter = Router()

bikeRouter
.route("/")    //localhost:8000/bikes
.post((req,res,next)=>{
    console.log(req.body)
    res.json("bike post")
})
.get((req,res,next)=>{
    res.json("bike get")
})
.patch ((req,res,next)=>{
    res.json("bike patch")
})
.delete((req,res,next)=>{
    res.json("bike delete")
})

bikeRouter
.route("/a/:id1/b/:id2")   //localhost:8000/a/ram/b/shyam
.post((req,res,next)=>{
    console.log(req.params)  //{ id1: 'ram', id2: 'shyam' }
    res.json("hello")
})

export default bikeRouter