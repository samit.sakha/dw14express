import { Router } from "express";
import { createCollege, deleteCollege, readAllCollege, readSpecificCollege, updateCollege } from "../productController/collegeController.js";
import upload from "../utils/upload.js";


let collegeRouter = Router()

collegeRouter
.route("/")
.post(upload.single("collegeImage"),createCollege)

.get(readAllCollege)

collegeRouter
  .route("/:id") //localhost:8000/college/any
  .get(readSpecificCollege)
  .patch(updateCollege)
  .delete(deleteCollege)

export default collegeRouter 