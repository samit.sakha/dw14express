import { Router } from "express";
import { Reviews } from "../schema/model.js";
import { createReview, deleteReview, readAllReview, readSpecificReview, updateReview } from "../productController/reviewController.js";


let reviewRouter = Router()

reviewRouter
.route("/")
.post(createReview)

.get(readAllReview)

reviewRouter
  .route("/:id") //localhost:8000/review/any
  .get(readSpecificReview)
  .patch(updateReview)
  .delete(deleteReview)

export default reviewRouter 