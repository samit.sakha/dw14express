import { Router } from "express";

let traineeRouter = Router()

traineeRouter
.route("/")
.post((req,res,next)=>{
    res.json({
        success: true,
        message: "trainee post successfully"
    })
})

.get((req,res,next)=>{
    res.json({
        success: true,
        message: "trainee get successfully"
    })
})

.patch((req,res,next)=>{
    res.join({
        success: true,
        message: "trainee patch successfully"
    })
})

.delete ((req,res,next)=>{
    res.join({
        success: true,
        message:"trainee delete successfully"
    })
})
export default traineeRouter