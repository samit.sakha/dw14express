import { Router } from "express";
import isAuthenticated from "../middleware/isAuthenticated.js";
import {
  createWebUser,
  deleteSpecificUser,
  forgotPassword,
  loginUser,
  myProfile,
  readAllWebUser,
  readSpecificUser,
  resetPassword,
  updatePassword,
  updateProfile,
  updateSpecificUser,
  verifyEmail,
} from "../productController/webUserController.js";
import authorized from "../middleware/authorized.js";

let webUserRouter = Router();

webUserRouter
  .route("/")
  .post(createWebUser)
  .get(isAuthenticated, authorized(["admin", "superadmin"]), readAllWebUser);

webUserRouter.route("/verifyEmail").patch(verifyEmail);

webUserRouter.route("/login").post(loginUser);

webUserRouter.route("/myProfile").get(isAuthenticated, myProfile);

webUserRouter.route("/updateProfile").patch(isAuthenticated, updateProfile);

webUserRouter.route("/updatePassword").patch(isAuthenticated, updatePassword);

webUserRouter.route("/forgotPassword").post(forgotPassword);

webUserRouter.route("/resetPassword").patch(isAuthenticated, resetPassword);

//below section should always be at the end
webUserRouter
  .route("/:id")
  .get(isAuthenticated, authorized(["admin", "superadmin"]), readSpecificUser)
  .patch(
    isAuthenticated,
    authorized(["admin", "superadmin"]),
    updateSpecificUser
  )
  .delete(isAuthenticated, authorized(["superadmin"]), deleteSpecificUser);

export default webUserRouter;
