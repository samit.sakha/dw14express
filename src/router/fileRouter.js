import { Router } from "express";
import { handleMultipleFile, handleSingleFile } from "../productController/fileController.js";
import upload from "../utils/upload.js";

let fileRouter = Router()

fileRouter.route("/single").post(upload.single("docs"),handleSingleFile)    //localhost:8000/file/single

fileRouter.route("/multiple").post(upload.array("docs"),handleMultipleFile)

export default fileRouter



/* 
upload.single("docs")
used to take from data
it takes file from postman and add to the public
it add file information to req.file
it add pther information to req.body
*/

/* ake a api  localhost:8000/file/multiple  , method post
send file from postman
save file to public
it gives gives links as response in array */