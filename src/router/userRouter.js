import { Router } from "express";
import { Users } from "../schema/model.js";
import { createUser, deleteUser, loginUser, readAllUser, readSpecificUser, updateUser } from "../productController/userController.js";


let userRouter = Router()

userRouter.route("/").post(createUser).get(readAllUser)


userRouter.route("/login").post(loginUser)

userRouter.route("/:id").get(readSpecificUser).patch(updateUser).delete(deleteUser) //localhost:8000/user/any

export default userRouter 