import { Router } from "express";
import { Products } from "../schema/model.js";
import { createProduct, deleteProduct, readAllProduct, readSpecificProduct, updateProduct } from "../productController/productController.js";

let productRouter = Router()

productRouter
.route("/")
.post(createProduct)

.get(readAllProduct)

productRouter
  .route("/:id") //localhost:8000/product/any
  .get(readSpecificProduct)
  .patch(updateProduct)
  .delete(deleteProduct)

export default productRouter 