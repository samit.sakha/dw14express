import { WebUser } from "../schema/model.js";

let authorized = (roles) => {
  return async (req, res, next) => {
    try {
      let _id = req._id;
      let result = await WebUser.findById(_id);
      let tokenRole = result.roles;

      if (roles.includes(tokenRole)) {
        next();
      } else {
        res.json({
          successful: false,
          message: "user not authorized",
        });
      }
    } catch (error) {
      res.json({
        success: false,
        message: "User is not authorized",
      });
    }
  };
};

export default authorized;
