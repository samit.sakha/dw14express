import envVar from "../utils/constant.js";
import jwt from "jsonwebtoken";

let isAuthenticated = async (req, res, next) => {
  try {
    let tokenString = req.headers.authorization;
    let tokenArray = tokenString.split(" ");
    let token = tokenArray[1];
    let user = await jwt.verify(token, envVar.secretKey);
    req._id = user._id;
    next();
  } catch (error) {
    res.json({
      success: false,
      message: "token not valid",
    });
  }
};

export default isAuthenticated;
 