import { WebUser } from "../schema/model.js";
import envVar from "../utils/constant.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { sendEmail } from "../utils/sendMail.js";

export let createWebUser = async (req, res, next) => {
  try {
    let data = req.body;
    let hashPassword = await bcrypt.hash(data.password, 10);
    data = {
      ...data,
      isVerifiedEmail: false,
      password: hashPassword,
    };

    let result = await WebUser.create(data);

    //send email with link
    // generate token
    let infoObj = {
      _id: result._id,
    };
    let expiryInfo = {
      expiresIn: "5d",
    };
    let token = await jwt.sign(infoObj, envVar.secretKey, expiryInfo);

    // send email
    await sendEmail({
      from: "Samit<samit.lads@gmail.com>",
      to: data.email,
      subject: "account create",
      html: `
      <h1>Dear ${data.fullName}, your account has been created successfully</h1>
      <a href="http://localhost:8000/verify-email?token=${token}">
      http://localhost:8000/verify-email?token=${token}
      </a>`,
    });

    res.json({
      success: true,
      message: "user created successdully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let verifyEmail = async (req, res, next) => {
  try {
    let tokenString = req.headers.authorization;
    let token = tokenString.split(" ")[1];

    let infoObj = await jwt.verify(token, envVar.secretKey);
    let userId = infoObj._id;

    let result = await WebUser.findByIdAndUpdate(
      userId,
      { isVerifiedEmail: true },
      { new: true }
    );
    res.status(200).json({
      success: true,
      message: "User Verified Successfully",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let loginUser = async (req, res, next) => {
  try {
    let email = req.body.email;
    let password = req.body.password;

    let user = await WebUser.findOne({ email: email });

    if (user) {
      if (user.isVerifiedEmail) {
        let isValidPassword = await bcrypt.compare(password, user.password);
        if (isValidPassword) {
          let infoObj = {
            _id: user._id,
          };
          let expiryInfo = {
            expiresIn: "365d",
          };
          let token = await jwt.sign(infoObj, envVar.secretKey, expiryInfo);
          res.json({
            success: true,
            message: "user login successful",
            data: token,
          });
        } else {
          let error = new Error("credential doesnot match");
          throw error;
        }
      } else {
        let error = new Error("credential doesnot match");
        throw error;
      }
    } else {
      let error = new Error("credential not found");
      throw error;
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let myProfile = async (req, res, next) => {
  try {
    let _id = req._id;
    let result = await WebUser.findById(_id);
    res.json({
      success: true,
      message: "profile read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: "unable to read profile",
    });
  }
};

export let updateProfile = async (req, res, next) => {
  try {
    let _id = req._id;
    let data = req.body;
    delete data.email;
    delete data.password;
    let result = await WebUser.findByIdAndUpdate(_id, data, { new: true });
    res.json({
      success: true,
      message: "profile updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updatePassword = async (req, res, next) => {
  try {
    let _id = req._id;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;

    let data = await WebUser.findById(_id);
    let hashPassword = data.password;
    let isValidPassword = await bcrypt.compare(oldPassword, hashPassword);

    if (isValidPassword) {
      let newHashPassword = await bcrypt.hash(newPassword, 10);
      let result = await WebUser.findByIdAndUpdate(
        _id,
        { password: newHashPassword },
        { new: true }
      );

      res.json({
        success: true,
        message: "password updated successfully",
        data: result,
      });
    } else {
      let error = new Error("Credential does not match");
      throw error;
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllWebUser = async (req, res, next) => {
  try {
    let result = await WebUser.find({});
    res.json({
      success: true,
      message: "all user read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificUser = async (req, res, next) => {
  try {
    let id = req.params.id;
    let result = await WebUser.findById(id);
    res.json({
      success: true,
      message: "user read successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateSpecificUser = async (req, res, next) => {
  try {
    let id = req.params.id;
    let data = req.body;
    delete data.email;
    delete data.password;

    let result = await WebUser.findByIdAndUpdate(id, data, { new: true });
    res.json({
      success: true,
      message: "user updated successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteSpecificUser = async (req, res, next) => {
  try {
    let id = req.params.id;

    let result = await WebUser.findByIdAndDelete(id);
    res.json({
      success: true,
      message: "user deleted successfully",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let forgotPassword = async (req, res, next) => {
  try {
    let email = req.body.email;
    let result = await WebUser.findOne({ email: email });

    if (result) {
      let infoObj = {
        _id: result._id,
      };
      let expiryInfo = {
        expiresIn: "5d",
      };
      let token = await jwt.sign(infoObj, envVar.secretKey, expiryInfo);

      // send email

      await sendEmail({
        from: "Samit<samit.lads@gmail.com>",
        to: email,
        subject: "reset password",
        html: `
      <h1>Click on the link to reset password</h1>
      <a href="http://localhost:8000/resetPassword?token=${token}">
      http://localhost:8000/resetPassword?token=${token}
      </a>`,
      });

      res.json({
        success: true,
        message: "reset link send successfull",
      });
    } else {
      res.json({
        success: false,
        message: "email not found",
      });
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let resetPassword = async (req, res, next) => {
  try {
    let _id = req._id;
    let hashPassword = await bcrypt.hash(req.body.password, 10);
    let result = await WebUser.findByIdAndUpdate(
      _id,
      { password: hashPassword },
      { new: true }
    );

    res.json({
      success: true,
      message: "password reset successful",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
