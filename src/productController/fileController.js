import envVar from "../utils/constant.js";

export const handleSingleFile = (req, res, next) => {
  // file info comes in req.file
  // other info comes in req.body
  console.log(req.body);
  console.log(req.file);
  let link = `${envVar.serverUrl}/${req.file.filename}`;
  res.json({
    success: true,
    message: "file uploaded successfully",
    result: link,
  });
};

export const handleMultipleFile = (req, res, next) => {
  console.log(req.files);

  let links = req.files.map((item, i) => {
    return `${envVar.serverUrl}/${item.filename}`;
  });
  res.json({
    success: true,
    message: "file uploaded successfully",
    result: links,
  });
};
