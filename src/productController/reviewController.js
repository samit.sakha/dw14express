import { Reviews } from "../schema/model.js";


export let createReview = async (req, res, next) => {
  let data = req.body;
  //save data to Reviews table
  try {
    let result = await Reviews.create(data);
    res.status(200).json({
      success: true,
      message: "review created successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllReview = async (req, res, next) => {
  try {
    let result = await Reviews.find({}).populate("product").populate("user")
    res.status(200).json({
      success: true,
      message: "review read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};


export let readSpecificReview = async (req, res, next) => {
        try {
          let result = await Reviews.findById(req.params.id);
          res.status(200).json({
            success: true,
            message: "review read successfully.",
            result: result,
          });
        } catch (error) {
          res.status(400).json({
            success: false,
            message: error.message,
          });
        }
}
      
export let updateReview = async (req, res, next) => {
        try {
          let result = await Reviews.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
          });
          res.status(200).json({
            success: true,
            message: "Review updated successfully.",
            result: result,
          });
        } catch (error) {
          res.status(400).json({
            success: false,
            message: error.message,
          });
        }
}
      
export let deleteReview = async (req, res, next) => {
        try {
          let result = await Reviews.findByIdAndDelete(req.params.id);
          res.status(200).json({
            success: true,
            message: "Review deleted successfully.",
            result: result,
          });
        } catch (error) {
          res.status(400).json({
            success: false,
            message: error.message,
          });
        }
}