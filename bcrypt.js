import bcrypt from "bcrypt"

// generate hash Password
// let ourPassword = "abc@1234"
// let hashedPassword = await bcrypt.hash(ourPassword, 10)
// console.log(hashedPassword)

//compare

let dbPassword ="$2b$04$5yNG1tNrprWL2jL2zKMlbebkcYLctCweJfR2gBE058KsMNyZeIe4y"
let isValidPassword= await bcrypt.compare("abc@1234", dbPassword)
console.log(isValidPassword)