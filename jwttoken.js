//jsonwebtoken

import jwt from "jsonwebtoken"

/* let info ={
    id:"12345435",
}

let secretKey="dw14"

let expiryInfo={
    expiresIn:"10s",
}

// expiryInfo must be in object with property that expires in after login

let token=jwt.sign(info, secretKey, expiryInfo)
console.log(token) */


let token ="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQ1NDM1IiwiaWF0IjoxNzE0NzA0MjU5LCJleHAiOjE3MTQ3MDQyNjl9.f1QGS03Frhg6GK8D2SCk8OigvN5G7kTxJMpO8gSxHEo"

try {
    let info =jwt.verify(token, "dw14")    
    console.log(info)
} catch (error) {
    console.log(error.message)
}

/*
condition to be verify
token must be made from give secret key
token must not expire



if token is verify
it gives info

else if token is not verify
it throw error

*/